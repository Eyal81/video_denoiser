import os
import shutil
import subprocess


def create_folder(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)


def del_folder(folder):
    shutil.rmtree(folder)


def video2audio(video_path, audio_path):
    cmd = f'ffmpeg -y -i "{video_path}" -vn -acodec pcm_s16le -ar 48000 -ac 2 "{audio_path}"'
    os.system(cmd)


def denoise_audio(noise_audio_path, clean_audio_dir):
    cmd = f'deepfilter.exe -o "{clean_audio_dir}" "{noise_audio_path}"'
    os.system(cmd)


def replace_audio(src_video_path, src_audio_path, dest_video_path):
    ffmpeg_cmd = [
        "ffmpeg",
        "-y",
        "-i", src_video_path,
        "-i", src_audio_path,
        "-c:v", "copy",
        "-c:a", "aac",
        "-map", "0:v:0",
        "-map", "1:a:0",
        "-strict", "experimental",
        dest_video_path
    ]

    try:
        subprocess.run(ffmpeg_cmd, check=True)
        print("Audio replaced successfully.")
    except subprocess.CalledProcessError as e:
        print("Error replacing audio:", e)
