import atexit
import concurrent.futures
import os
import shutil
import customtkinter
from tkinter import filedialog, messagebox

from utils import (
    create_folder,
    del_folder,
    denoise_audio,
    replace_audio,
    video2audio,
)

TMP_FOLDER = "tmp"
create_folder(TMP_FOLDER)


class App(customtkinter.CTk):
    def __init__(self):
        super().__init__()

        self.file_paths = None
        self.clean_video_paths = []
        self.title("Video Denoiser")
        self.geometry(f"{500}x{350}")

        label = customtkinter.CTkLabel(
            self,
            text="Video Denoiser",
            fg_color="transparent",
            text_color=("gray10", "#DCE4EE"),
            font=customtkinter.CTkFont(size=28),
        )
        label.place(relx=0.5, rely=0.1, anchor=customtkinter.S)

        label = customtkinter.CTkLabel(
            self,
            text="This program is a Low Complexity Speech\nEnhancement Framework for Full-Band Audio (48kHz)\nusing "
                 "on Deep Filtering",
            fg_color="transparent",
            justify="left",
            text_color=("gray10", "#DCE4EE"),
            font=customtkinter.CTkFont(size=16),
        )
        label.place(relx=0.5, rely=0.3, anchor=customtkinter.S)

        label = customtkinter.CTkLabel(
            self,
            text="Insert MP4 file to process:",
            fg_color="transparent",
            justify="left",
            text_color=("gray10", "#DCE4EE"),
            font=customtkinter.CTkFont(size=20),
        )
        label.place(relx=0.02, rely=0.4, anchor=customtkinter.W)

        button = customtkinter.CTkButton(
            self, text="Select MP4 File", command=self.select_video
        )
        button.place(relx=0.02, rely=0.49, anchor=customtkinter.W)

        self.mp4_label = customtkinter.CTkLabel(
            self,
            fg_color="transparent",
            justify="left",
            text_color=("gray10", "#DCE4EE"),
            font=customtkinter.CTkFont(size=16),
        )
        self.mp4_label_enable = False

        button = customtkinter.CTkButton(
            self, text="Start denoising!", command=self.denoise_video
        )
        button.place(relx=0.02, rely=0.58, anchor=customtkinter.W)

        self.progressbar = customtkinter.CTkProgressBar(self, orientation="horizontal")

        self.download_button = customtkinter.CTkButton(
            self,
            text="Download denoised Video",
            command=self.download_video,
            state="disabled",
        )
        self.download_button.place(relx=0.9, rely=0.9, anchor=customtkinter.E)

    def select_video(self):
        self.file_paths = filedialog.askopenfilenames(
            title="Select mp4 files", filetypes=[("*.mp4", "*.MP4")]
        )
        if self.file_paths:
            print("Selected MP4 files:", self.file_paths)
            if not self.mp4_label_enable:
                self.mp4_label_enable = True
                self.mp4_label.place(relx=0.4, rely=0.49, anchor=customtkinter.W)
            self.mp4_label.configure(
                text=f"Selected MP4 files: {', '.join(self.file_paths)}"
            )

    def denoise_video(self):
        if not self.mp4_label_enable:
            messagebox.showinfo("Alert", "Please select a file.")
        else:
            self.progressbar.place(relx=0.40, rely=0.58, anchor=customtkinter.W)
            self.progressbar.start()

            with concurrent.futures.ThreadPoolExecutor() as executor:
                # Process each video file concurrently
                futures = [executor.submit(self.process_single_video, file_path) for file_path in self.file_paths]
                concurrent.futures.wait(futures)

            self.download_button.configure(state="normal")
            self.progressbar.stop()
            messagebox.showinfo("Alert", "Done!")

    def process_single_video(self, file_path):
        basename = os.path.basename(file_path)
        # Extract audio from the video
        video_audio_path = f"{TMP_FOLDER}/video_audio_{basename}.wav"
        video2audio(file_path, video_audio_path)

        # Denoise audio
        clean_audio_path = f"{TMP_FOLDER}/clean/video_audio_{basename}.wav"
        denoise_audio(video_audio_path, f"{TMP_FOLDER}/clean")

        # Replace video noise audio with clean one
        clean_video_path = f"{TMP_FOLDER}/clean/video_{basename}.mp4"
        replace_audio(file_path, clean_audio_path, clean_video_path)

        self.clean_video_paths.append(clean_video_path)

    def download_video(self):
        destination_folder_path = filedialog.askdirectory(
            title="Select destination folder"
        )
        for clean_video_path in self.clean_video_paths:
            if clean_video_path and destination_folder_path:
                try:
                    shutil.copy(clean_video_path, destination_folder_path)
                    messagebox.showinfo("Alert", "Done!")
                except Exception as e:
                    messagebox.showinfo("Alert", f"Error: {e}")
            else:
                messagebox.showinfo(
                    "Alert", "Please select source file and destination folder."
                )


def del_tmp_folder():
    del_folder(TMP_FOLDER)


atexit.register(del_tmp_folder)

app = App()
app.mainloop()
